$(document).ready(function () {
   $('#update-roles-modal').iziModal();
   $('#add-new-role-modal').iziModal();
   $(document).on('click', '.update-roles-modal', function(){
       let username = this.getAttribute('data-username');
       let role = this.getAttribute('data-role');
       $('#update-roles-modal').iziModal('setSubtitle', username);
       $('#update-roles-modal').iziModal('open');
       get_user_role_data(username, role);
    });
   $(document).on('click', '.add-role-modal', function () {
       get_all_roles();
       $('#add-new-role-modal').iziModal('setSubtitle', '');
       $('#add-new-role-modal').iziModal('open');
   });
   $(document).on('click', '#updateRole', function () {
        let newRole = document.getElementById('userRole').value;
        let username = document.getElementById('userRole').getAttribute('data-username');
        let email = document.getElementById('userEmail').value;
        update_email(username, email);
        update_users_role(username, newRole);
   });
   $(document).on('click', '.updateRole', function () {
        let roleToUpdate = this.getAttribute('data-role-name');
        updateRolePermissions(roleToUpdate)
   });
   $(document).on('click', '#addRole', function () {
       let roleToAdd = this.getAttribute('data-role-name');
       addNewRole(roleToAdd);
   });
   $(document).on('click', '.deleteRole', function(){
      let roleToDelete = this.getAttribute('data-role-name');
      deleteRole(roleToDelete);
   });
   $(document).on('click', '#cancelAddRole', function () {
       $('#add-new-role-modal').iziModal('close');
   });
   $(document).on('click', '#cancelRole', function () {
       $('#update-roles-modal').iziModal('close');
   })
});

function deleteRole(roleToDelete){
    let data = {'api_token': document.getElementById('api_token').value}
    $.ajax({
        url: '/r/'+roleToDelete,
        method: 'DELETE',
        data: data,
        success: function (_, _, xhr) {
            if (xhr.status == 200){
                $('#add-new-role-modal').iziModal('setSubtitle', 'Successfully deleted ' + roleToDelete);
                get_all_roles()
            } else {
                $('#add-new-role-modal').iziModal('setSubtitle', 'Failed to delete ' + roleToDelete + ', Error code: ' + xhr.status.toString());
            }
        }
    });
}

function addNewRole(roleToAdd) {
    let roleData = document.getElementById(roleToAdd).children;
    let title = roleData[0].firstChild.value;
    let data = {};
    for (let i = 1; i < roleData.length-1; i++) {
        let input = roleData[i].firstChild;
        let key = input.name;
        let value = input.checked;
        if (value){
            data[key] = true;
        } else {
            data[key] = false;
        }
    }
    data['api_token'] = document.getElementById('api_token').value;
    $.ajax({
        url: '/r/' + title,
        method: 'POST',
        data: data,
        success: function (data, _, xhr) {
            if (xhr.status === 201) {
                $('#add-new-role-modal').iziModal('setSubtitle', 'Successfully added ' + title);
                get_all_roles()
            } else {
                $('#add-new-role-modal').iziModal('setSubtitle', 'Failed to add ' + title + '. Error code ' + xhr.status.toString());
            }
        }
    });
}

function updateRolePermissions(role){
    let updatedRoleData = document.getElementById(role).children;
    let title = updatedRoleData[0].firstChild.textContent;
    let data = {};
    for (let i = 1; i < updatedRoleData.length-1; i++){
        let input = updatedRoleData[i].firstChild;
        let key = input.name;
        let value = input.checked;
        if (value){
            data[key] = true;
        } else {
            data[key] = false;
        }
    }
    data['api_token'] = document.getElementById('api_token').value;
    $.ajax({
        url: '/r/'+title.toString(),
        method: 'PUT',
        data: data,
        success: function (data, _, xhr) {
            if (xhr.status === 200){
                $('#add-new-role-modal').iziModal('setSubtitle', 'Successfully updated '+title);
            } else {
                $('#add-new-role-modal').iziModal('setSubtitle', 'Failed to update ' + title + '. Error code ' + xhr.status.toString());
            }
        }
    });
}

function get_all_roles(){
    $.ajax({
        url: '/r',
        method: 'GET',
        success: function (data) {
            let tableBody = document.getElementById('roleTableBody');
            while (tableBody.firstChild) {
                tableBody.removeChild(tableBody.firstChild);
            }
            data['data'].forEach(role => {
                let newRow = document.createElement('tr');
                newRow.id = role.title;
                newRow.setAttribute('data-role-name', role.title);
                let nameEl = document.createElement('td');
                nameEl.appendChild(document.createTextNode(role.title));
                let readEl  = document.createElement('td');
                let writeEl = document.createElement('td');
                let updateEl = document.createElement('td');
                let deleteEl  = document.createElement('td');
                let adminEl  = document.createElement('td');
                let updateButtonEl = document.createElement('td');
                let deleteButtonEl = document.createElement('td');
                let readModEl = document.createElement('input');
                let writeModEl = document.createElement('input');
                let updateModEl = document.createElement('input');
                let deleteModEl= document.createElement('input');
                let adminModEl = document.createElement('input');
                readModEl = applyCheck(readModEl, role.can_read, 'can_read');
                writeModEl = applyCheck(writeModEl, role.can_write, 'can_write');
                updateModEl = applyCheck(updateModEl, role.can_update, 'can_update');
                deleteModEl = applyCheck(deleteModEl, role.can_delete, 'can_delete');
                adminModEl = applyCheck(adminModEl, role.is_admin, 'is_admin');
                readEl.appendChild(readModEl);
                writeEl.appendChild(writeModEl);
                updateEl.appendChild(updateModEl);
                deleteEl.appendChild(deleteModEl);
                adminEl.appendChild(adminModEl);
                let updateButtonModEl = document.createElement('button');
                updateButtonModEl.className += 'updateRole';
                updateButtonModEl.setAttribute('data-role-name', role.title);
                updateButtonModEl.appendChild(document.createTextNode('Update'));
                updateButtonEl.appendChild(updateButtonModEl);
                let deleteButtonModEl = document.createElement('button');
                deleteButtonModEl.className += 'deleteRole';
                deleteButtonModEl.setAttribute('data-role-name', role.title);
                deleteButtonModEl.appendChild(document.createTextNode('Delete'));
                deleteButtonEl.appendChild(deleteButtonModEl);
                newRow.appendChild(nameEl);
                newRow.appendChild(readEl);
                newRow.appendChild(writeEl);
                newRow.appendChild(updateEl);
                newRow.appendChild(deleteEl);
                newRow.appendChild(adminEl);
                newRow.appendChild(updateButtonEl);
                newRow.appendChild(deleteButtonEl);
                tableBody.appendChild(newRow);
            });
            let newRow = document.createElement('tr');
            newRow.id = 'newRoleAdd';
            newRow.setAttribute('data-role-name', 'newRoleAdd');
            let nameEl = document.createElement('td');
            let readEl  = document.createElement('td');
            let writeEl = document.createElement('td');
            let updateEl = document.createElement('td');
            let deleteEl  = document.createElement('td');
            let adminEl  = document.createElement('td');
            let updateButtonEl = document.createElement('td');
            let nameModEl = document.createElement('input')
            let readModEl = document.createElement('input');
            let writeModEl = document.createElement('input');
            let updateModEl = document.createElement('input');
            let deleteModEl= document.createElement('input');
            let adminModEl = document.createElement('input');
            readModEl = applyCheck(readModEl, false, 'can_read');
            writeModEl = applyCheck(writeModEl, false, 'can_write');
            updateModEl = applyCheck(updateModEl, false, 'can_update');
            deleteModEl = applyCheck(deleteModEl, false, 'can_delete');
            adminModEl = applyCheck(adminModEl, false, 'is_admin');
            nameEl.appendChild(nameModEl);
            readEl.appendChild(readModEl);
            writeEl.appendChild(writeModEl);
            updateEl.appendChild(updateModEl);
            deleteEl.appendChild(deleteModEl);
            adminEl.appendChild(adminModEl);
            let updateButtonModEl = document.createElement('button');
            updateButtonModEl.id = 'addRole';
            updateButtonModEl.setAttribute('data-role-name', 'newRoleAdd');
            updateButtonModEl.appendChild(document.createTextNode('Add'));
            updateButtonEl.appendChild(updateButtonModEl);
            newRow.appendChild(nameEl);
            newRow.appendChild(readEl);
            newRow.appendChild(writeEl);
            newRow.appendChild(updateEl);
            newRow.appendChild(deleteEl);
            newRow.appendChild(adminEl);
            newRow.appendChild(updateButtonEl);
            tableBody.appendChild(newRow);
        }
    });
}

function applyCheck(element, data, type){
    if (data === true){
        element.type = 'checkbox';
        element.checked = true;
        element.name = type;
        element.setAttribute('data-role-value', true)
    } else {
        element.type = 'checkbox';
        element.name = type;
        element.setAttribute('data-role-value', false)
    }
    return element
}

function get_user_role_data(username, role){
    $.ajax({
        url: '/u',
        method: 'GET',
        data: {'username': username},
        success: function (data) {
            data = data['data'][0];
            console.log(data)
            document.getElementById('userEmail').value = data.email;
        }
    });
    $.ajax({
        url: '/r',
        method: 'GET',
        success: function (data) {
            let select = document.getElementById('userRole');
            select.setAttribute('data-username', username);
            while (select.firstChild) {
                select.removeChild(select.firstChild);
            }
            data['data'].forEach(i => {
                let roleName = i.title;
                let newOption = document.createElement('option');
                if(role == roleName) {
                    newOption.appendChild(document.createTextNode(roleName + ' (Current)'));
                    newOption.selected = true;
                } else {
                    newOption.appendChild(document.createTextNode(roleName));
                }
                newOption.value = roleName;
                select.appendChild(newOption);
            })
        }
    });
}

function update_users_role(username, role) {
    let api_token = document.getElementById('api_token').value;
    $.ajax({
        url: '/u/' + username + '/role',
        method: 'PUT',
        data: {'value': role, 'api_token': api_token},
        success: function (_, _, xhr) {
            if (xhr.status == 201){
                window.location.reload(true);
            } else {
                let errorStatus = document.createTextNode(xhr.status);
                document.getElementById('roleUpdateStatus').appendChild(errorStatus);
            }
        }
    });
}

function update_email(username, email) {
    let api_token = document.getElementById('api_token').value;
    $.ajax({
        url: '/u/' + username + '/email',
        method: 'PUT',
        data: {'value': email, 'api_token': api_token},
        success: function (_, _, xhr) {
            if (xhr.status != 201){
                let errorStatus = document.createTextNode(xhr.status);
                document.getElementById('roleUpdateStatus').appendChild(errorStatus);
            }
        }
    });
}

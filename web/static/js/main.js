$(document).ready(function () {
    let username = document.getElementById('username').value;
    getProfileInfo(username);
    getApiToken();
});

function getProfileInfo(username) {

    $.ajax({
        url: '/u',
        method: 'GET',
        data: {'username': username},
        success: function (data) {
            data = data['data'][0]
            document.getElementById('email').value = data['email']
        }
    });
}

function getApiToken() {
    $.ajax({
        url: '/profile/apiKey',
        method: 'GET',
        success: function (data) {
            document.getElementById('apiKey').appendChild(document.createTextNode(data));
        }
    })
}

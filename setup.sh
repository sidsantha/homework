#!/bin/bash


KEY=$(uuidgen)
TOKEN=$(uuidgen)
mkdir config
mkdir db
echo -e "SECRET_KEY = '$KEY' \nWEB_INTERFACE_TOKEN = '$TOKEN'" > config/dev.py
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt --proxy http://proxy-chain.intel.com:911
python3 app.py

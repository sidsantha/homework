# Requirements
Python 3.7 or later, venv, and linux
Linux make sure you have python3-venv installed before running the setup script then follow the steps below.

# Setup (Linux)

 1. Unzip the no-docker-master.zip file into the directory you would like to run the application from.
 2. Ensure the setup.sh script is executable (chmod +x setup.sh) and run ./setup.sh
 3. You should see the application configure itself and install the required python libraries.
 4. Once it is complete you will see a request to navigate in your browser to http://localhost:5000/first this will allow you to set up the initial admin account for the application. (If you are setting up the application remotely please replace localhost with the remove machines IP address).
 5. Set up your initial account and enjoy.
 6. You may wish to disconnect the running process from the terminal as initial setup will run it as an active process.
 
# Starting Server post setup:
You can run the start-server.sh script (which will start the server in the background) or the following commands:
source venv/bin/activate
python3 app.py

# API Documentation
To use the api you will need to first navigate to your profile (located at the navigation bar at the top of the screen AFTER you have logged in).
On this page you will be able to change your email address and see your API_TOKEN, you will need to pass as a key value pair into routes noted below.
Any routes that are italicized require a value 

## Overview
User Routes:

 - `/u`
	 - You may use a GET request on this route to access data for users in the application.
	 - You may pass key/value pairs for user data (example: {'email': 'test@example.com'}) to filter user by matching values.
 - `/u/<username>`
	 - This POST route is used to create a new account it has two required keys that must contain values, *password* and *email*. WARNING the current demonstration version runs under http and is not secure, do not pass real usernames or passwords into the application, while the application salts and hashes all passwords they are transmitted over the network in plain text.
 - `/u/<username>` **API_TOKEN REQUIRED**
	 - These PUT and DELETE routes can be used to modify user data and delete a user account. A user may only modify/delete there own account, accounts marked as admin may modify or delete any user account. 
 - `/u/<username>/<setting>`
	 - The GET route can be used to get a single piece of information from a user account.
	 - The PUT and DELETE routes **API_TOKEN REQUIRED** are used to update or clear information for a single user setting.
 - `/auth`
	 - The auth route determines if a user can login.
 - `/r`
	 - This GET route can be used to get information about account roles and what types of permission they have.
 - `/r/<role>`
	 - This POST route can be used to create a new role, the only required key value pair is {'title': '`<newrolename>`'}. Though this will cause the role to have no permissions.
	 - This PUT route can be used to update the settings on a role. **API_TOKEN with ADMIN PRIVILEGES required**
	 - This DELETE route can be used to delete a role. **API_TOKEN with ADMIN PRIVILEGES required**
 - `/r/<role>/<setting>`
	 - This GET route can be used to access the value of a specific roles setting.
	 - This PUT route can be used to update the value of a specific roles setting.  **API_TOKEN with ADMIN PRIVILEGES required**
	 - This DELETE route can be used to set a specific roles setting back to default (the lowest privilege level. **API_TOKEN with ADMIN PRIVILEGES required**

## Web UI
The web ui is fairly self explanatory, only admin accounts can update users form the ui, users may view all users from the home page once logged in.

## Quirks

 - Currently password updating is disallowed, please don't forget your
   password. 
- If an admin removes the 'is_admin' from there own role via
   the web interface the change will not take effect until the next
   login in order to prevent the admin from accidentally locking
   themselves out.
from flask_login import UserMixin
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, Binary
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, aliased
from sqlalchemy import create_engine

BASE = declarative_base()
ENGINE = create_engine('sqlite:///db/db.sqlite3')

SESSION = sessionmaker(bind=ENGINE)


class Role(BASE):
    __tablename__ = 'role'
    id = Column(Integer, primary_key=True)
    title = Column(String(32), nullable=False)
    can_read = Column(Boolean, default=False, nullable=False)
    can_write = Column(Boolean, default=False, nullable=False)
    can_update = Column(Boolean, default=False, nullable=False)
    can_delete = Column(Boolean, default=False, nullable=False)
    is_admin = Column(Boolean, default=False, nullable=False)


class User(BASE, UserMixin):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(128), nullable=False)
    password = Column(Binary, nullable=False)
    email = Column(String(128))
    api_token = Column(String(36), nullable=False)
    last_login = Column(DateTime, nullable=False)
    role_id = Column(Integer, ForeignKey('role.id'))
    role = relationship(Role)


class FirstTimeLogin(BASE):
    __tablename__ = 'first_time_login'
    id = Column(Integer, primary_key=True)
    code = Column(String(36), nullable=False)
    is_used = Column(Boolean, nullable=False)


BASE.metadata.create_all(ENGINE)

from flask import request
from flask_restful import Resource
from bin.api_methods.individual_user_account_methods import get_user_accounts, post_new_user_accounts, \
    update_individual_user_account, delete_individual_user_account
from bin.api_methods.user_setting_methods import get_user_setting, delete_user_setting, put_user_setting
from bin.api_methods.login_methods import post_login
from bin.api_methods.role_check_methods import get_roles, post_role, put_role, delete_role
from bin.api_methods.role_settings import get_role_setting, put_role_setting, delete_role_setting


class UserAccount(Resource):
    def get(self):
        data = request.values.to_dict(flat=True)
        return get_user_accounts(data)


class IndividualUserAccount(Resource):
    def post(self, username):
        data = request.values.to_dict(flat=True)
        return post_new_user_accounts(username, data)

    def put(self, username):
        print('hit')
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'PUT'
        return update_individual_user_account(username, data)

    def delete(self, username):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'DELETE'
        return delete_individual_user_account(username, data)


class UserSettings(Resource):
    def get(self, username, setting):
        return get_user_setting(username, setting)

    def put(self, username, setting):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'PUT'
        return put_user_setting(username, setting, data)

    def delete(self, username, setting):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'DELETE'
        return delete_user_setting(username, setting, data)


class LoginUser(Resource):
    def post(self):
        data = request.values.to_dict(flat=True)
        return post_login(data)


class RoleCheckSingle(Resource):
    def get(self):
        data = request.values.to_dict(flat=True)
        return get_roles(data)


class RoleCheck(Resource):
    def post(self, role):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'ADMIN'
        return post_role(role, data)

    def put(self, role):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'ADMIN'
        return put_role(role, data)

    def delete(self, role):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'ADMIN'
        return delete_role(role, data)


class RoleSetting(Resource):
    def get(self, role, setting):
        return get_role_setting(role, setting)

    def put(self, role, setting):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'ADMIN'
        return put_role_setting(role, setting, data)

    def delete(self, role, setting):
        data = request.values.to_dict(flat=True)
        data['route_type'] = 'ADMIN'
        return delete_role_setting(role, setting, data)

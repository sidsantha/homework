from bin.classes.db_models import SESSION, Role, User, FirstTimeLogin
from sqlalchemy.exc import StatementError, IntegrityError
from bcrypt import hashpw, gensalt
from uuid import uuid4
from datetime import datetime


def check_if_first_run():
    s = SESSION()
    q = s.query(FirstTimeLogin).first()
    if not q:
        uuid = str(uuid4())
        first_login = FirstTimeLogin(code=uuid, is_used=False)
        s.add(first_login)
        s.commit()
        s.close()
        print(f'Welcome please go to http://localhost:5000/first to create the admin account.')
        return
    elif q.is_used is False:
        print(f'Welcome back, please create the admin account by going to: http://localhost:5000/first')
        s.close()
        return
    else:
        s.close()
        return


def get_initial_uuid():
    s = SESSION()
    q = s.query(FirstTimeLogin).filter(FirstTimeLogin.is_used == False).first()
    s.close()
    if not q:
        return False
    uuid = q.code
    return uuid


def make_initial_admin_and_roles(username, password, email, uuid):
    default_role_admin = Role(title='Admin', can_read=True, can_write=True, can_update=True, can_delete=True,
                              is_admin=True)
    default_role_user = Role(title='User', can_read=True, can_write=False, can_update=False, can_delete=False,
                             is_admin=False)
    s = SESSION()
    s.add(default_role_admin)
    s.add(default_role_user)
    q = s.query(FirstTimeLogin).filter(FirstTimeLogin.code == uuid).first()
    q.is_used = True
    s.commit()
    s.close()
    register_user(username, password, email, 'Admin', timestamp=datetime.now())
    login_data = login_user(username, password)
    return login_data


def register_user(username, password, email, role, timestamp):
    api_token = str(uuid4())
    s = SESSION()
    q = s.query(User).filter(User.username == username).first()
    if not q:
        hashed_password = hashpw(password.encode('utf-8'), gensalt(rounds=12))
        r = s.query(Role).filter(Role.title == role).first()
        new_user = User(username=username, password=hashed_password, email=email, last_login=timestamp, role=r,
                        api_token=api_token)
        s.add(new_user)
        s.commit()
        s.close()
        return True
    else:
        s.close()
        return False


# Log User In
def login_user(username, password):
    s = SESSION()
    q = s.query(User).filter(User.username == username).first()
    if not q:
        s.close()
        return False
    hashed_password = hashpw(password.encode('utf-8'), q.password)
    if hashed_password == q.password:
        if q.role.title == 'Admin':
            s.close()
            return q.username, q.id, True
        else:
            s.close()
            return q.username, q.id, False
    s.close()
    return False


def get_user():
    users = {
        'data': []
    }
    s = SESSION()
    q = s.query(User).all()
    for row in q:
        user = {
            'username': row.username,
            'id': row.id,
            'email': row.email,
            'last_login': row.last_login,
            'role': row.role.title
        }
        users['data'].append(user)
    s.close()
    return users


def get_custom_user(data):
    s = SESSION()
    q = s.query(User).all()
    matching_users = {
        'data': []
    }
    for row in q:
        for k in data.keys():
            if k in row.__dict__ and data[k] == row.__dict__[k] or k == 'role' and data[k] == row.role.title:
                user = {
                    'id': row.id,
                    'username': row.username,
                    'email': row.email,
                    'last_login': row.last_login,
                    'role': row.role.title
                }
                matching_users['data'].append(user)
    s.close()
    return matching_users


def get_custom_user_value(username, setting):
    if setting == 'api_token' or setting == 'password':
        return False
    elif setting == 'role':
        s = SESSION()
        q = s.query(User).filter(User.username == username).first()
        role = q.role.title
        s.close()
        data = {setting: role}
        return data
    else:
        s = SESSION()
        q = s.query(User).filter(User.username == username).first()
        all_settings = {k: v for k, v in q.__dict__.items() if not k.startswith('__') and not callable(k)}
        data = {setting: all_settings[setting]}
        s.close()
        return data


def change_setting(username, setting, value):
    s = SESSION()
    try:
        q = s.query(User).filter(User.username == username).first()
        if q is not None and setting != 'role':
            setattr(q, setting, value)
            s.commit()
            s.close()
            return True
        elif q is not None and setting == 'role':
            r = s.query(Role).filter(Role.title == value).first()
            if r is not None:
                q.role_id = r.id
                s.commit()
                s.close()
                return True
            else:
                return False
        else:
            return False
    except StatementError:
        s.close()
        return False


def set_setting_to_null(username, setting):
    s = SESSION()
    try:
        q = s.query(User).filter(User.username == username).first()
        if q is not None:
            value = None
            setattr(q, setting, value)
            s.commit()
            s.close()
            return True
        else:
            return False
    except IntegrityError:
        s.close()
        return False


def get_user_permissions_from_token(api_token):
    s = SESSION()
    q = s.query(User).filter(User.api_token == api_token).first()
    try:
        role = s.query(Role).filter(Role.title == q.role.title).first()
        data = {
            'user_token': q.api_token,
            'can_read': role.can_read,
            'can_write': role.can_write,
            'can_update': role.can_update,
            'can_delete': role.can_delete,
            'is_admin': role.is_admin
        }
        s.close()
        return data
    except AttributeError:
        s.close()
        return False


def get_user_token(username):
    s = SESSION()
    q = s.query(User).filter(User.username == username).first()
    s.close()
    if q is not None:
        return q.api_token
    else:
        return False


def update_user_data(username, data):
    try:
        del data['username']
        del data['id']
        del data['role']
    except KeyError:
        pass
    s = SESSION()
    q = s.query(User).filter(User.username == username).first()
    for k, v in data.items():
        setattr(q, k, v)
    s.commit()
    s.close()
    return True


def delete_user_account(username, data):
    s = SESSION()
    s.query(User).filter(User.username == username).delete()
    s.commit()
    s.close()
    return True


def get_all_roles():
    s = SESSION()
    q = s.query(Role).all()
    all_roles = {
        'data': []
    }
    for row in q:
        role = {
            'id': row.id,
            'title': row.title,
            'can_read': row.can_read,
            'can_write': row.can_write,
            'can_update': row.can_update,
            'can_delete': row.can_delete,
            'is_admin': row.is_admin
        }
        all_roles['data'].append(role)
    s.close()
    return all_roles


def get_filtered_roles(data):
    s = SESSION()
    q = s.query(Role).all()
    filtered_roles = {
        'data': []
    }
    for row in q:
        for k, v in row.__dict__.items():
            try:
                if data[k] == v:
                    role = {
                        'id': row.id,
                        'title': row.title,
                        'can_read': row.can_read,
                        'can_write': row.can_write,
                        'can_update': row.can_update,
                        'can_delete': row.can_delete,
                        'is_admin': row.is_admin
                    }
                    if frozenset(role) not in filtered_roles:
                        filtered_roles['data'].append(role)
            except KeyError:
                pass
    s.close()
    return filtered_roles


def post_new_role(role, data):
    new_role = Role(title=role)
    for k, v in data.items():
        setattr(new_role, k, v)
    s = SESSION()
    q = s.query(Role).filter(Role.title == role).first()
    if not q:
        s.add(new_role)
        s.commit()
        s.close()
        return True
    else:
        return False


def put_update_role(role, data):
    s = SESSION()
    q = s.query(Role).filter(Role.title == role).first()
    for k, v in data.items():
        if k == 'id':
            pass
        elif k == 'title':
            r = s.query(Role).filter(Role.title == role).all()
            count = 0
            for row in r:
                if row.title == role:
                    count += 1
            if count > 1:
                s.close()
                return False
            setattr(q, k, v)
        else:
            setattr(q, k, v)
    s.commit()
    s.close()
    return True


def delete_delete_role(role, data):
    s = SESSION()
    q = s.query(Role).filter(Role.title == role).first()
    if q is not None:
        s.query(Role).filter(Role.title == role).delete()
        s.commit()
        s.close()
        return True
    else:
        s.close()
        return False


def get_get_role_setting(role, setting):
    s = SESSION()
    try:
        q = s.query(Role).filter(Role.title == role).first()
        if q is not None:
            all_settings = {k: v for k, v in q.__dict__.items() if not k.startswith('__') and not callable(k)}
            data = {setting: all_settings[setting]}
            s.close()
            return data
        else:
            s.close()
            return False
    except KeyError:
        s.close()
        return False


def put_put_role_setting(role, setting, data):
    s = SESSION()
    q = s.query(Role).filter(Role.title == role).first()
    try:
        if q is not None:
            setattr(q, setting, data['value'])
            s.commit()
            s.close()
            return True
        else:
            s.close()
            return False
    except KeyError:
        s.close()
        return False


def delete_delete_role_setting(role, setting):
    s = SESSION()
    q = s.query(Role).filter(Role.title == role).first()
    if q is not None:
        if setting != 'id' or setting != 'title':
            setattr(q, setting, False)
            s.commit()
            s.close()
            return True
        else:
            s.close()
            return False
    else:
        s.close()
        return False


def get_api_key_for_user(username):
    s = SESSION()
    q = s.query(User).filter(User.username == username).first()
    if q is not None:
        s.close()
        return q.api_token
    else:
        s.close()
        return None

from sqlalchemy.exc import StatementError, IntegrityError
from bin.db_methods import get_user_permissions_from_token, get_user_token
from config.dev import WEB_INTERFACE_TOKEN


def token_check(func):
    def wrapper_function(*args, **kwargs):
        for n, i in enumerate(args):
            if isinstance(i, dict):
                try:
                    token = args[n]['api_token']
                    if token == WEB_INTERFACE_TOKEN:
                        del args[n]['api_token']
                        pass
                    else:
                        # TODO: Refactor to user role permissions
                        role_data = get_user_permissions_from_token(token)
                        if args[n]['route_type'] == 'ADMIN':
                            if role_data['is_admin']:
                                pass
                            else:
                                return 'Unauthorized', 401
                        else:
                            user_token = get_user_token(args[0])
                        if args[n]['route_type'] == 'PUT' and \
                                (role_data['can_write'] or role_data['can_update']) and \
                                role_data['is_admin']:
                            pass
                        elif args[n]['route_type'] == 'DELETE' and \
                                role_data['can_delete'] and \
                                role_data['is_admin']:
                            pass
                        elif args[n]['route_type'] == 'PUT' and \
                                (role_data['can_write'] or role_data['can_update']) and \
                                role_data['is_admin'] is False and \
                                token == user_token:
                            pass
                        elif args[n]['route_type'] == 'PUT' and \
                                (role_data['can_write'] or role_data['can_update']) and \
                                role_data['is_admin'] is False and \
                                token != user_token:
                            return 'Unauthorized', 401
                        else:
                            return 'Unauthorized', 401
                except KeyError or IndexError:
                    return 'Error missing api_token.', 401
        return func(*args, **kwargs)
    return wrapper_function


def bool_check(func):
    def wrapper_function(*args, **kwargs):
        for n, i in enumerate(args):
            if isinstance(i, dict):
                for k, v in args[n].items():
                    if v in ['true', 'True', 'TRUE']:
                        args[n][k] = True
                    if v in ['false', 'False', 'FALSE']:
                        args[n][k] = False
        return func(*args, **kwargs)
    return wrapper_function


def bad_request_check(func):
    def wrapper_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeyError or IndexError or StatementError or IntegrityError or TypeError as e:
            return 'Bad Request', 400
    return wrapper_function

from flask import jsonify
from bin.db_methods import register_user, get_all_users
from bin.api_methods.util import bad_request_check
from datetime import datetime


@bad_request_check
def get_all_user_accounts():
    users = get_all_users()
    return jsonify(users)


@bad_request_check
def post_new_user_accounts(data):
    timestamp = datetime.now()
    success = register_user(data['username'], data['password'], data['email'], 'User', timestamp)
    if success:
        return 'Successfully registered new user!', 201
    else:
        return 'Bad Request', 400

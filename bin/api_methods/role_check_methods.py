from flask import jsonify
from bin.api_methods.util import token_check, bool_check, bad_request_check
from bin.db_methods import get_all_roles, get_filtered_roles, post_new_role, put_update_role, delete_delete_role


@bool_check
@bad_request_check
def get_roles(data):
    if not bool(data):
        roles = get_all_roles()
        return jsonify(roles)
    else:
        return jsonify(get_filtered_roles(data))


@token_check
@bool_check
@bad_request_check
def post_role(role, data):
    success = post_new_role(role, data)
    if success:
        return f'Successfully created role: {role}.', 201
    else:
        return 'Bad request.', 400


@token_check
@bool_check
@bad_request_check
def put_role(role, data):
    success = put_update_role(role, data)
    if success:
        return f'Successfully updated role: {role}.', 200
    else:
        return 'Title must be unique.', 400


@token_check
@bad_request_check
def delete_role(role, data):
    success = delete_delete_role(role, data)
    if success:
        return 'Success.', 200
    else:
        print('failure')
        return 'Bad Request.', 400

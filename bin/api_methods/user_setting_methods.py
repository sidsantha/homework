from flask import jsonify
from bin.db_methods import get_custom_user_value, set_setting_to_null, change_setting
from bin.api_methods.util import token_check, bad_request_check


@bad_request_check
def get_user_setting(username, setting):
    setting_data = get_custom_user_value(username, setting)
    if setting_data:
        return jsonify(setting_data)
    else:
        return 'Forbidden', 403


@token_check
@bad_request_check
def put_user_setting(username, setting, data):
    value = data['value']
    success = change_setting(username, setting, value)
    if success:
        return 'User setting changed successfully.', 201
    else:
        return 'Bad Request', 400


@token_check
@bad_request_check
def delete_user_setting(username, setting, data):
    success = set_setting_to_null(username, setting)
    if success:
        return f'User setting: {setting} deleted successfully.', 200
    else:
        return 'Bad Request.', 400

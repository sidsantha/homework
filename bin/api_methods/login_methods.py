from flask import jsonify
from bin.db_methods import login_user
from bin.api_methods.util import bad_request_check


@bad_request_check
def post_login(data):
    can_login = login_user(data['username'], data['password'])
    if can_login:
        data = {'name': can_login[0], 'id': can_login[1], 'is_admin': can_login[2]}
        return jsonify(data)
    else:
        return 'Error unauthorized.', 401

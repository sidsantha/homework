from flask import jsonify
from bin.db_methods import get_user, get_custom_user, update_user_data, delete_user_account, register_user
from bin.api_methods.util import token_check, bad_request_check, bool_check
from datetime import datetime


@bad_request_check
@bool_check
def get_user_accounts(data):
    if not data:
        users = get_user()
        return jsonify(users)
    else:
        users = get_custom_user(data)
        if type(users) is dict:
            return jsonify(users)
        else:
            return users


@bad_request_check
def post_new_user_accounts(username, data):
    timestamp = datetime.now()
    success = register_user(username, data['password'], data['email'], 'User', timestamp)
    if success:
        return 'Successfully registered new user!', 201
    else:
        return 'Bad Request', 400


@token_check
@bad_request_check
def update_individual_user_account(username, data):
    success = update_user_data(username, data)
    if success:
        message = 'Successfully updated user data.', 201
    else:
        message = 'Unable to update user data.', 400
    return message


@token_check
@bad_request_check
def delete_individual_user_account(username, data):
    success = delete_user_account(username, data)
    if success:
        message = 'Successfully deleted user account.', 200
    else:
        message = 'Unable to delete user account.', 400
    return message

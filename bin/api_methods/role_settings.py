from flask import jsonify
from bin.api_methods.util import token_check, bool_check, bad_request_check
from bin.db_methods import get_get_role_setting, put_put_role_setting, delete_delete_role_setting


def get_role_setting(role, setting):
    success = get_get_role_setting(role, setting)
    if success:
        return jsonify(success)
    else:
        return 'Bad request.', 400


@token_check
@bool_check
@bad_request_check
def put_role_setting(role, setting, data):
    success = put_put_role_setting(role, setting, data)
    if success:
        return f'Successfully updated {role}', 201
    else:
        return 'Bad Request', 400


@token_check
@bad_request_check
def delete_role_setting(role, setting, data):
    success = delete_delete_role_setting(role, setting)
    if success:
        return f'Successfully deleted {setting} value.', 200
    else:
        return 'Bad Request', 400

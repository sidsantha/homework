from flask import Flask
from flask import render_template,request, redirect, session, jsonify
from flask_login import LoginManager, login_required, logout_user
from flask_restful import Api
from config.dev import SECRET_KEY, WEB_INTERFACE_TOKEN
from bin.classes.db_models import User
from bin.classes.api import IndividualUserAccount, LoginUser, UserSettings, RoleCheck, RoleSetting, UserAccount, \
    RoleCheckSingle
from bin.db_methods import check_if_first_run, make_initial_admin_and_roles, get_initial_uuid, get_api_key_for_user
from datetime import datetime
import requests
import json

app = Flask(__name__,
            static_url_path='',
            static_folder='web/static',
            template_folder='web/templates')
# TODO:change for production
# Enable for Debug
app.config.update(
            TEMPLATES_AUTO_RELOAD=True
)
app.config['SECRET_KEY'] = SECRET_KEY
api = Api(app)

login_manager = LoginManager()
login_manager.init_app(app)

# Check if initial admin account has been created:
check_if_first_run()

# Web routes
@app.route('/', methods=['GET'])
@login_required
def index():
    users = requests.get('http://localhost:5000/u')
    users = json.loads(users.text)
    return render_template('index.html', user_data=users)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        session.clear()
        logout_user()
        return render_template('login.html')
    elif request.method == 'POST':
        data = request.values
        success = requests.post('http://localhost:5000/userlogin', data=data)
        data = json.loads(success.text)
        if success.status_code == 401:
            return render_template('login.html', flash=True)
        elif success.status_code == 200:
            session['user_id'] = data['id']
            session['name'] = data['name']
            session['is_admin'] = data['is_admin']
            session['logged_in'] = True
            session['api_token'] = WEB_INTERFACE_TOKEN
            last_seen_url = 'http://localhost:5000/u/' + data['name'] + '/last_login'
            last_seen_data = {
                'api_token': WEB_INTERFACE_TOKEN,
                'value': datetime.now()
            }
            requests.put(last_seen_url, data=last_seen_data)
            return redirect('/')
        else:
            return render_template('login.html', flash=True)


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    if request.method == 'GET':
        return render_template('profile.html')
    else:
        email_update = request.values['email']
        url = 'http://localhost:5000/u/'+session['name']+'/email'
        data = {'value': email_update, 'api_token': WEB_INTERFACE_TOKEN}
        requests.put(url, data=data)
        return redirect('/profile')


@app.route('/profile/apiKey', methods=['GET'])
@login_required
def api_key():
    username = session['name']
    user_api_key = get_api_key_for_user(username)
    return user_api_key


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    elif request.method == 'POST':
        data = {'password': request.values['password'], 'email': request.values['email']}
        username = request.values['username']
        success = requests.post('http://localhost:5000/u/' + username, data=data)
        if success:
            data = request.values
            user = requests.post('http://localhost:5000/userlogin', data=data)
            user = json.loads(user.text)
            if success.status_code == 401 or success.status_code == 400:
                return render_template('login.html', flash=True)
            elif success.status_code == 201:
                session['user_id'] = user['id']
                session['name'] = user['name']
                session['is_admin'] = user['is_admin']
                session['logged_in'] = True
                last_seen_url = 'http://localhost:5000/u/' + user['name'] + '/last_login'
                last_seen_data = {
                    'api_token': WEB_INTERFACE_TOKEN,
                    'value': datetime.now()
                }
                requests.put(last_seen_url, data=last_seen_data)
                return redirect('/')
        else:
            return 'broken'


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    session.clear()
    logout_user()
    return redirect('/login')


@app.route('/first', methods=['GET', 'POST'])
def initial_admin_creation():
    session.clear()
    logout_user()
    uuid = get_initial_uuid()
    if uuid:
        if request.method == 'GET':
            return render_template('register.html', first_time=True, uuid=uuid)
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']
            email = request.form['email']
            data = make_initial_admin_and_roles(username, password, email, uuid)
            # TODO: Clean this up, should be using api
            session['user_id'] = data[1]
            session['name'] = data[0]
            session['is_admin'] = data[2]
            session['logged_in'] = True
            return redirect('/')
    else:
        return redirect('/')


api.add_resource(UserAccount,
                 '/u'
                 )
api.add_resource(IndividualUserAccount,
                 '/u/<username>'
                 )
api.add_resource(UserSettings,
                 '/u/<username>/<setting>'
                 )
api.add_resource(LoginUser,
                 '/userlogin'
                 )
api.add_resource(RoleCheckSingle,
                 '/r')
api.add_resource(RoleCheck,
                 '/r/<role>'
                 )
api.add_resource(RoleSetting,
                 '/r/<role>/<setting>'
                 )

# Create User Account


# Update User Account
#
# Destroy User Accounts

# Login Handlers
@login_manager.user_loader
def load_user(username):
    user = User()
    user.username = username
    return user


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/login')


@app.errorhandler(404)
def page_not_found(e):
    error_type = '404'
    return render_template('error.html', type=error_type, data=e), 404


@app.errorhandler(500)
def internal_server_error(e):
    error_type = '500'
    return render_template('error.html', type=error_type, data=e), 500


@app.errorhandler(403)
def forbidden(e):
    error_type = '403'
    return render_template('error.html', type=error_type, data=e), 403


@app.errorhandler(400)
def bad_request(e):
    error_type = '400'
    return render_template('error.html', type=error_type, data=e), 400


@app.errorhandler(401)
def unauthorized(e):
    error_type = '401'
    return render_template('error.html', type=error_type, data=e), 401


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
